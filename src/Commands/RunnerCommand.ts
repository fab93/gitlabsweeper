import { Command, OptionValues } from "commander";
import { GroupIdOption } from "../Options/GroupIdOption";
import { RunnerPrefixOption } from "../Options/RunnerPrefixOption";
import { ResourceSweeper } from "../ResourceSweeper";

export class RunnerCommand extends Command {
  constructor() {
    super("runners");
    this.description("Delete offline gitlab runners");
    this.command("runners");
    this.addOption(new GroupIdOption());
    this.addOption(new RunnerPrefixOption());

    this.action((_, options: OptionValues) => {
      const baseUrl = options.gitlabUrl;
      const sweeper = new ResourceSweeper();
      sweeper.sweepResources(
        baseUrl,
        "runners",
        options.gitlabToken,
        options.dryRun,
        options.deletionCountLimit
      );
    });
  }
}
