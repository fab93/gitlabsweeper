import { Option } from "commander";

export class DeletionLimitOption extends Option {
  constructor() {
    super(
      "-l, --deletion-limit <deletionLimit>",
      "The maximum number of resources to delete"
    );
  }
}
