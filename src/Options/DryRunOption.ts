import { Option } from "commander";

export class DryRunOption extends Option {
  constructor() {
    super(
      "--dry-run",
      "Whether to actually delete the resources or just log them"
    );
  }
}
