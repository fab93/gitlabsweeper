import { Option } from "commander";

export class GitLabBaseUrlOption extends Option {
  constructor() {
    super(
      "--gitlab-base-url <gitlabBaseUrl>",
      "Gitlab url to use for every request"
    );

    this.makeOptionMandatory();
  }
}
