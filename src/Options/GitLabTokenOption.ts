import { Option } from "commander";

export class GitLabTokenOption extends Option {
  constructor() {
    super(
      "-t, --gitlab-token <gitlabToken>",
      "Gitlab token which has read and write access to the requested resources"
    );
    this.makeOptionMandatory();
  }
}
