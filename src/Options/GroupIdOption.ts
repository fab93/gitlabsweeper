import { Option } from "commander";

export class GroupIdOption extends Option {
  constructor() {
    super("-p, --group-id <groupId>", "The group id to operate on");
  }
}
