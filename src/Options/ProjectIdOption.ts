import { Option } from "commander";

export class ProjectIdOption extends Option {
  constructor() {
    super("-p, --project-id <projectId>", "The project id to operate on");
  }
}
