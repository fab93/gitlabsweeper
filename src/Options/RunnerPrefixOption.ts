import { Option } from "commander";

export class RunnerPrefixOption extends Option {
  constructor() {
    super("--runner-prefix", "Only deletes runners with the specified prefix");
  }
}
