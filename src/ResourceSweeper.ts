import { debugLog } from "./logging.util";

const config: { [deletionType: string]: any } = {
  branches: {
    getUrlPrefix: (baseUrl: string, projectId: string) =>
      `${baseUrl}/projects/${projectId}/repository/branches`,
    getDeletionUrlPrefix: (baseUrl: string, projectId: string) =>
      `${baseUrl}/projects/${projectId}/repository/branches/`,
    getName: (branch: any) => branch.name,
    getDate: (branch: any) => branch.commit.authored_date,
    getId: (branch: any) => branch.name,
    shouldBeDeleted: (toDelete: any, daysAgo: number) =>
      (toDelete.default === true || toDelete.protected === true) &&
      daysAgo > 365 / 2,
  },
  tags: {
    getUrlPrefix: (baseUrl: string, projectId: string) =>
      `${baseUrl}/projects/${projectId}/repository/tags`,
    getDeletionUrlPrefix: (baseUrl: string, projectId: string) =>
      `${baseUrl}/projects/${projectId}/repository/tags/`,
    getName: (tag: any) => tag.name,
    getId: (tag: any) => tag.name,
    getDate: (tag: any) => tag.commit.authored_date,
    shouldBeDeleted: (toDelete: any, daysAgo: number) =>
      toDelete.protected === false && daysAgo > 365 / 2,
  },
  packages: {
    getUrlPrefix: (baseUrl: string, projectId: string) =>
      `${baseUrl}/projects/${projectId}/packages`,
    getDeletionUrlPrefix: (baseUrl: string, projectId: string) =>
      `${baseUrl}/projects/${projectId}/packages/`,
    getName: (pkg: any) => pkg.version,
    getId: (pkg: any) => pkg.id,
    getDate: (pkg: any) => pkg.created_at,
    shouldBeDeleted: (toDelete: any, daysAgo: number) =>
      toDelete.package_type === "generic" && daysAgo > 60,
  },
  runners: {
    getUrlPrefix: (baseUrl: string, groupId: string) =>
      `${baseUrl}/groups/${groupId}/runners`,
    getDeletionUrlPrefix: (baseUrl: string) => `${baseUrl}/runners/`,
    getName: (runner: any) => runner.description,
    getId: (runner: any) => runner.id,
    getDate: (runner: any) => runner.created_at,
    shouldBeDeleted: (toDelete: any, prefix: string) => {
      return (
        toDelete.online === false && toDelete.description.startsWith(prefix)
      );
    },
  },
} as const;

export class ResourceSweeper {
  private currentPage: number = 1;
  private deletionCount: number = 0;

  public async sweepResources(
    urlPrefix: string,
    deletionType: keyof typeof config,
    gitlabToken: string,
    dryRun: boolean,
    deletionCountLimit: number
  ) {
    const url = `${urlPrefix}?per_page=100&page=${this.currentPage}`;
    debugLog(`Fetching ${deletionType} with url ${url}`);

    const response = await fetch(url, {
      headers: {
        "PRIVATE-TOKEN": gitlabToken,
      },
    });

    debugLog("Got response", response.status, response.statusText);

    if (!response.ok) {
      console.error(
        `Failed to fetch ${deletionType}: ${response.status} ${response.statusText}`
      );
      process.exit(1);
    }

    const objects = await response.json();
    debugLog(`Got ${objects.length} ${deletionType}`);
    let deletedOnPage = 0;

    for (const toDelete of objects) {
      const updatedDate = new Date(config[deletionType].getDate(toDelete));
      const timeDiff = Date.now() - updatedDate.getTime();
      const daysAgo = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

      if (!config[deletionType].shouldBeDeleted(toDelete, daysAgo)) {
        continue;
      }

      const name = config[deletionType].getName(toDelete);
      const id = config[deletionType].getId(toDelete);
      const deletionUrl = `${config[deletionType].deletionUrlPrefix}${id}`;
      console.log(
        `${
          dryRun ? "[DRY RUN]: " : ""
        }Deleting ${name} with id ${id} with url ${deletionUrl}`
      );
      debugLog(toDelete);

      if (dryRun !== true) {
        const deleteResponse = await fetch(deletionUrl, {
          method: "DELETE",
          headers: {
            "PRIVATE-TOKEN": gitlabToken,
          },
        });

        if (!deleteResponse.ok) {
          console.error(
            `Failed to delete "${name}": ${deleteResponse.status} ${deleteResponse.statusText}`
          );
        } else {
          debugLog(`Deleted "${name}"`);
          deletedOnPage++;
        }
      }

      this.deletionCount++;
      if (this.deletionCount >= deletionCountLimit) {
        console.log(`Reached deletion limit of ${deletionCountLimit}`);
        console.log(
          `${dryRun ? "[DRY RUN]: " : ""}Deleted ${
            this.deletionCount
          } ${deletionType}`
        );
        process.exit(0);
      }
    }
    if (objects.length > 0) {
      if (deletedOnPage === 0) {
        this.currentPage++;
      }
      await this.sweepResources(
        urlPrefix,
        deletionType,
        gitlabToken,
        dryRun,
        deletionCountLimit
      );
    } else {
      console.log(`No more ${deletionType} to delete`);
      console.log(
        `${dryRun ? "[DRY RUN]: " : ""}Deleted ${
          this.deletionCount
        } ${deletionType}`
      );
      process.exit(0);
    }
  }
}
