import { program } from "commander";
import { RunnerCommand } from "./Commands/RunnerCommand";
import { DeletionLimitOption } from "./Options/DeletionLimitOption";
import { DryRunOption } from "./Options/DryRunOption";
import { GitLabBaseUrlOption } from "./Options/GitLabBaseUrlOption";
import { GitLabTokenOption } from "./Options/GitLabTokenOption";

program
  .name("gitlab-sweeper")
  .addOption(new GitLabTokenOption())
  .addOption(new DeletionLimitOption())
  .addOption(new DryRunOption())
  .addOption(new GitLabBaseUrlOption())
  .addCommand(new RunnerCommand())
  .parse();
